from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.form import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.urls import reverse


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser = request.user)
    context = {
        "receipts": receipts
    }
    return render(request, "receipts/list.html", context)

def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form
    }

    return render(request, "receipts/create.html", context)

def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    category_data = []

    for category in categories:
        receipt_count = Receipt.objects.filter(category = category).count()
        category_data.append({
            "name": category.name,
            "receipt_count": receipt_count,
        })
    context = {
        "category_data": category_data
    }
    return render(request,"receipts/categories.html", context )


def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    account_data = []

    for account in accounts:
        receipt_count = Receipt.objects.filter(account= account).count()
        account_data.append({
            "name": account.name,
            "number": account.number,
            "receipt_count": receipt_count,
        })

    context = {
        "account_data": account_data
    }
    return render(request,"receipts/accounts.html", context)



@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context={
        "form": form
    }
    return render(request, "receipts/createcategory.html", context)


@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit = False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create_account.html", context)


